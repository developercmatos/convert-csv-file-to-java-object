# convert-csv-file-to-java-object



## Descrição

Esse projeto tem o objetivo de mostrar um exemplo de conversão de  conteúdo de um arquivo csv em uma lista de objeto java.

## Tecnologias utilizadas
- Java 11+
- Maven

## Bibliotecas utilizadas
- [Lombok](https://mvnrepository.com/artifact/org.projectlombok/lombok)
- [Jackson-DataFormat-Csv](https://mvnrepository.com/artifact/com.fasterxml.jackson.dataformat/jackson-dataformat-csv)


