package conversor;


import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;


import java.io.File;
import java.io.IOException;
import java.util.List;

public class LeitorConversorDeArquivo {

    /**
     * Método genérico para transformar um conteúdo de um arquivo csv em uma lista de objeto Java
     */
    public <T> List<T> converterCsvFileComHeaderEmObjetoJava(String path, Class<T> classe) throws IOException {
        File csvFile = new File(path);

        CsvMapper csvMapper = new CsvMapper();

        CsvSchema csvSchema = csvMapper
                .typedSchemaFor(classe)
                .withHeader()
                .withColumnSeparator(',')
                .withComments();

        MappingIterator<T> objIter = csvMapper
                .readerFor(classe)
                .with(csvSchema)
                .readValues(csvFile);

        return objIter.readAll();
    }

}
