package dto;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

/**
 * A annotation @JsonPropertyOrder serve para deixar as colunas
 * do resultado na mesma ordem que está no arquivo csv
 */
@Data
@JsonPropertyOrder({"nome", "preco", "categoria"})
public class Produto {

    private String nome;
    private String preco;
    private String categoria;

}
