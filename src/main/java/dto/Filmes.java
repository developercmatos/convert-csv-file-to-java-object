package dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

/**
 * A annotation @JsonPropertyOrder serve para deixar as colunas
 * do resultado na mesma ordem que está no arquivo csv
 */
@Data
@JsonPropertyOrder({"titulo", "genero", "ano_lancamento"})
public class Filmes {

    private String titulo;
    private String genero;
    private String ano_lancamento;

}
