import conversor.LeitorConversorDeArquivo;
import dto.Filmes;
import dto.Produto;

import java.io.IOException;
import java.util.List;

public class MainValidate {

    public static void main(String[] args) throws IOException {

        LeitorConversorDeArquivo leitor = new LeitorConversorDeArquivo();
        /**
         * Exemplos de conversão do conteúdo csv em uma lista de objetos Java
         */
        List<Produto> lista = leitor.converterCsvFileComHeaderEmObjetoJava("src/main/java/file/arquivo.csv", Produto.class);
        lista.forEach(System.out::println);


        List<Filmes> listaFilmes = leitor.converterCsvFileComHeaderEmObjetoJava("src/main/java/file/arquivo2.csv", Filmes.class);
        listaFilmes.forEach(System.out::println);

    }
}
